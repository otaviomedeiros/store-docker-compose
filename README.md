# Store Microservices Dependencies

Runs dependencies for all micro services for dev environments.

##### Run dependencies

```
docker-compose up -d
```

##### Run all micro services

```
docker-compose -f services.docker-compose.yml -f docker-compose.yml up -d
```

##### Run test dependencies

```
docker-compose -f test.docker-compose.yml up -d
```
